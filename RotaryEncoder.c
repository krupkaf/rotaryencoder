#include "RotaryEncoder.h"
#include <avr/interrupt.h>

volatile int16_t rotaryEncoderX = 0;
volatile int16_t rotaryEncoderXMax = 32767;
volatile int16_t rotaryEncoderXMin = -32768;
volatile uint8_t changed = 0;

#if defined(AVRCD324) || defined(AVRCD644) || defined(AVRCD1284)
// PC4  -  Vcc
// PC5  -  SW
// PC6  -  DT
// PC7  -  CLK

#define testSW ((PINC & (1 << PC5)) != 0)
#define testDT ((PINC & (1 << PC6)) != 0)
#define testCLK ((PINC & (1 << PC7)) != 0)

void rotaryEncoderInit(void) {
  TCCR2A = 0x02;
  OCR2A = (F_CPU / 32) / 2500;
  TCCR2B = 0x03; //  clkT2S/32
  TIMSK2 |= 1 << OCF2A;

  DDRC &= 0x0f;
  DDRC |= 0x10;
  PORTC |= 0xF0;
}
#endif

#ifdef RS485_LCDDisplay070917
// PB2  -  SW
// PB0  -  DT
// PB1  -  CLK

#define testSW ((PINB & (1 << PB2)) != 0)
#define testDT ((PINB & (1 << PB0)) != 0)
#define testCLK ((PINB & (1 << PB1)) != 0)

void rotaryEncoderInit(void) {
  TCCR2A = 0x02;
  OCR2A = (F_CPU / 32) / 2500;
  TCCR2B = 0x03; //  clkT2S/32
  TIMSK2 |= 1 << OCF2A;

  DDRB &= 0xF8;
  PORTB |= 0x0;
}
#endif

SIGNAL(TIMER2_COMPA_vect) {
  /**Aktualni stav vstupu*/
  unsigned char In;
  /**Stav vstupu pri minulem preruseni - testovani*/
  static unsigned char PrevIn = 0;
  /**Detekovane hrany, 1 - hrana, 0 - bezezmeny*/
  unsigned char Edge;
  /**Maskovani hran (sumu)*/
  static unsigned char EdgeMask = 0x01;
  /**Diference o kterou se bude posouvat x*/
  signed char d;
  /**Minule znamenko d*/
  static signed PrevSgnD = 0;
  /**Pocitani casu pro vetsi skoky v editovane hodnote */
  static unsigned char t = 0;
  int16_t tmpX;

  if (t < 255) {
    t++;
  }

  In = (!testDT ? 0x01 : 0x00) | (!testCLK ? 0x02 : 0x00) |
       (!testSW ? 0x04 : 0x00);

  // button
  if (In & 0x04) {
    GPIOR0 |= (1 << 0);
    changed = 1;
  } else {
    GPIOR0 &= ~(1 << 0);
  };

  Edge = In ^ PrevIn;
  PrevIn = In;
  Edge &= EdgeMask;
  if (Edge) {
    EdgeMask =
        0x03 ^ Edge; // Pristi hrana muze byt detekovana mouze na druhem vstupu
  }
  if ((EdgeMask & 0x03) == 0) {
    // Nekdo nastavil masku hran na nejaky nesmysl a kouslo by se to.
    EdgeMask = 0x01;
  }

  if (Edge & 0x01) { // x budu menit pouze pokud jde o hnahu na vstupu 0. Pri
                     // hrane na vstupu 1 se x NEmeni
    Edge ^= In; // Edge.0 == 0 - sestupna hrana, Edge.0 == 1 - vzestupna hrana.
    if (Edge & 0x01) { // Reaguji pouze na nabeznou hranu
      changed = 1;
      In >>= 1; // Posunu si bit 1 na bit 0
      Edge ^= In;
      if (Edge & 0x01) {
        // Na vstupu 0 je sestupna hrana a zaroven na vstupu 1 je 1
        // nebo na vstupu 0 je vzestupna hrana a zaroven na vstupu 1 je 0
        d = -1;
      } else {
        // A obracene.
        d = 1;
      }
      if (PrevSgnD != d) {
        t = 255;
      }
      PrevSgnD = d;
      d = 1;
      if (t < 50) {
        d = 100;
      }
      if (PrevSgnD == -1) {
        d = -d;
      }
      tmpX = rotaryEncoderX;
      tmpX += d;
      if (tmpX > rotaryEncoderXMax) {
        tmpX = rotaryEncoderXMax;
      }
      if (tmpX < rotaryEncoderXMin) {
        tmpX = rotaryEncoderXMin;
      }
      rotaryEncoderX = tmpX;
      t = 0;
    };
  };
}

int16_t rotaryEncoderGet(void) {
  uint8_t _SREG = SREG;
  cli();
  uint16_t tmp = rotaryEncoderX;
  SREG = _SREG;
  return tmp;
}

void rotaryEncoderSet(int16_t newVal) {
  uint8_t _SREG = SREG;
  cli();
  rotaryEncoderX = newVal;
  SREG = _SREG;
}

void rotaryEncoderMinMax(int16_t min, int16_t max) {
  uint8_t _SREG = SREG;
  cli();
  rotaryEncoderXMin = min;
  if (rotaryEncoderX < rotaryEncoderXMin) {
    rotaryEncoderX = rotaryEncoderXMin;
  }
  rotaryEncoderXMax = max;
  if (rotaryEncoderX > rotaryEncoderXMax) {
    rotaryEncoderX = rotaryEncoderXMax;
  }
  SREG = _SREG;
}

uint8_t rotaryEncoderChanged(void) {
  uint8_t tmp = changed;
  changed = 0;
  return tmp;
}
