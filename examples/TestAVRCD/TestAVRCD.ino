#include "RotaryEncoder.h"

void setup() {
        Serial.begin(115200);
        rotaryEncoderInit();
        rotaryEncoderXMax = 1000;
        rotaryEncoderXMin = -1000;
}

void loop() {
        for(;;) {
                Serial.print(F("Button:"));
                Serial.print(rotaryEncoderButton);
                Serial.print(F(" Position:"));
                Serial.println(rotaryEncoderX);
                delay(75);
        }
}
