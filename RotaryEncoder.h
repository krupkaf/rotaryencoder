#ifndef ROTARYENCODER_H_
#define ROTARYENCODER_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
extern volatile int16_t rotaryEncoderX;
extern volatile int16_t rotaryEncoderXMax;
extern volatile int16_t rotaryEncoderXMin;

#define rotaryEncoderButton ((GPIOR0 & 0x01))

void rotaryEncoderInit(void);
int16_t rotaryEncoderGet(void);
void rotaryEncoderSet(int16_t newVal);
void rotaryEncoderMinMax(int16_t min, int16_t max);
uint8_t rotaryEncoderChanged(void);
#define rotaryEncoderWaitToUp() while (rotaryEncoderButton);

#ifdef __cplusplus
}
#endif

#endif
